import random


def display_the_letter(word, users_letters, word_overviev):
    """Verify the positions of the letter entered by the player in the word."""
    for position_w in range(0, len(word)):
        for letter in range(0, len(users_letters)):
            if users_letters[letter] == word[position_w]:
                word_overviev[position_w] = word[position_w] + " "
    return word_overviev


def letter_check(users_letters, word, attempts):
    """This function checks the validity of the user's input,
    such as ensuring it is a single letter, hasn't been guessed before,
    and whether it is present in the word or not.
    It also handles updating the number of attempts left."""
    user_input = input("Please, enter one letter: ").upper()
    if not user_input.isalpha():
        print("You can only enter the letters from English alphabeth.")
    elif len(user_input) > 1:
        print("You can only enter one letter at time.")
    elif user_input in users_letters:
        print("You have allready entered that letter.")
    elif user_input not in word:
        print("\n", "*" * 20)
        print("The letter is not in the word.")
        print("*" * 20)
        attempts -= 1
    else:
        users_letters.append(user_input)
    return users_letters, attempts


def hangman_draw(attempts):
    """This function displays
    the hangman figure based on the number of attempts left."""
    hangman_at = ["""-------
 |   |
 O   |
/|\  |
/ \  |
     |
--------""",
"""-------
 |   |
 O   |
/|\  |
     |
     |
--------""",
"""-------
 |   |
 O   |
     |
     |
     |
--------""",
"""-------
     |
     |
     |
     |
     |
--------""",
"""
     |
     |
     |
     |
     |
--------""",
"""
--------"""]
    print(hangman_at[attempts])


def main():
    """This is the main function of the Hangman game.
    It initializes the game, handles user input,
    and controls the flow of the game until the game is won or lost."""
    words_list = ["Air", "Music", "Tree", "Street", "Spam", "Bryan"]
    word = random.choice(words_list).upper()
    attempts = 5
    users_letters = []  # A list containing letters guessed by the user.
    word_overviev = []  # A list containing the current state of the word, with correctly guessed letters revealed.
    for letters in word:
        word_overviev.append("_ ")
    print(f"Welcome to Hangman (or Hangwoman;)!!!")
    print(f"Your word is {len(word)} letters long. Start guessing!")

    while True:
        print("Your word: " + "".join(word_overviev))
        users_letters, attempts = letter_check(users_letters, word, attempts)
        word_overviev = display_the_letter(word, users_letters, word_overviev)
        hangman_draw(attempts)
        if attempts == 0:
            print("\n", "*" * 20)
            print("\nYou loose!")
            print("\n", "*" * 20)
            break
        elif "_ " not in word_overviev:
            print("\n", "*" * 20)
            print("\nYou won!!!")
            print("\n", "*" * 20)
            break

while True:
    main()
    play_again = input("Do You want to play again? Y/N: ").upper()
    if play_again == "Y":
        print("Here we go again!")
    else:
        break
